# e_store 0.0.9 (Mai 17, 2019)

* Add `bitbucket-pipelines.yml`

  *Marcelo Toledo*

## e_store 0.0.8 (Apr 25, 2019)

* TDD: Added Index to Order
* Renamed some described tests
* Update README.md
* Added seeds

  *Marcelo Toledo*

## e_store 0.0.7 (Apr 25, 2019)

* TDD: Added Total Order Service
* TDD: Added Average Ticket Service

  *Marcelo Toledo*

## e_store 0.0.6 (Apr 25, 2019)

* Added new test to product not found

  *Marcelo Toledo*

## e_store 0.0.5 (Apr 25, 2019)

* TDD: Added factory to order and validations to stock
* Somes fixes
* Created a specific exception to stock control
* Added ENUM to Status of the order

  *Marcelo Toledo*

## e_store 0.0.4 (Apr 21, 2019)

* TDD: Added tests to do Create Order Model
* Added Item Model
* Added items to order
* Added Service to create an order

  *Marcelo Toledo*

## e_store 0.0.3 (Apr 20, 2019)

* TDD: Added tests to do CRUD of Customer Model
* Fixed CodeClimate config

  *Marcelo Toledo*

## e_store 0.0.2 (Apr 19, 2019)

* TDD: Added tests to do CRUD of Product Model
* Added CodeClimate config

  *Marcelo Toledo*

## e_store 0.0.1 (Apr 19, 2019)

* Created project
* Configured project
* Added docker
* Configured gems

  *Marcelo Toledo*
