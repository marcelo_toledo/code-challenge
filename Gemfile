source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.0'

gem 'bootsnap', '~> 1.4', '>= 1.4.2', require: false
gem 'coffee-rails', '~> 4.2', '>= 4.2.2'
gem 'jbuilder', '~> 2.8'
gem 'kaminari', '~> 1.1', '>= 1.1.1'
gem 'pg', '~> 1.1', '>= 1.1.4'
gem 'puma', '~> 3.12', '>= 3.12.1'
gem 'rack-cors', '~> 1.0', '>= 1.0.3'
gem 'rails', '~> 5.2', '>= 5.2.3'
gem 'sass-rails', '~> 5.0', '>= 5.0.7'
gem 'turbolinks', '~> 5.2'
gem 'uglifier', '~> 4.1', '>= 4.1.20'

# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  gem 'database_cleaner', '~> 1.7'
  gem 'factory_bot_rails', '~> 5.0', '>= 5.0.1'
  gem 'faker', '~> 1.9', '>= 1.9.3'
  gem 'pry-byebug', '~> 3.7'
  gem 'rspec-rails', '~> 3.8', '>= 3.8.2'
  gem 'rubocop'
  gem 'rubocop-performance'
  gem 'rubycritic'
  gem 'shoulda-matchers', '~> 4.0', '>= 4.0.1'
  gem 'simplecov'
end

group :development do
  gem 'brakeman'
  gem 'listen', '~> 3.1', '>= 3.1.5'
  gem 'spring', '~> 2.0', '>= 2.0.2'
  gem 'spring-watcher-listen', '~> 2.0', '>= 2.0.1'
  gem 'web-console', '~> 3.7'
end
